import React, { Component } from "react";
import {
  Button,
  Col,
  Container,
  Form,
  Input,
  InputGroup,
  Row
} from "reactstrap";
import data from "./data";
class Reg extends Component {
  state = {
    name: "",
    email: "",
    password: ""
  };

  Name = event => {
    this.setState({ name: event.target.value });
  };
  Email = event => {
    this.setState({ email: event.target.value });
  };
  Password = event => {
    this.setState({ password: event.target.value });
  };
  register = event => {
    event.preventDefault();
    data.push(this.state);
    this.setState({
      name: "",
      email: "",
      password: ""
    });
    console.log(data);
  };
  render() {
    return (
      <div className="app flex-row align-items-center">
        <Container>
          <Row className="justify-content-center">
            <Col md="9" lg="7" xl="6">
              <Form>
                <div className="row" className="mb-2 pageheading">
                  <div className="col-sm-12 btn btn-primary">Sign Up Page</div>
                </div>
                <InputGroup className="mb-3">
                  <Input
                    type="text"
                    onChange={this.Name}
                    placeholder="Name"
                    name="name"
                    value={this.state.name}
                  />
                </InputGroup>
                <InputGroup className="mb-3">
                  <Input
                    type="text"
                    onChange={this.Email}
                    name="email"
                    placeholder="Email"
                  />
                </InputGroup>
                <InputGroup className="mb-3">
                  <Input
                    type="password"
                    onChange={this.Password}
                    name="password"
                    placeholder="Password"
                  />
                </InputGroup>
                <Button onClick={this.register} color="success" block>
                  Create Account
                </Button>
              </Form>
            </Col>
          </Row>
        </Container>
        <ul>
          {data.map(dat => {
            return (
              <React.Fragment key={dat.name}>
                <li>{dat.name}</li>
                <li>{dat.email}</li>
                <li>{dat.password}</li>
              </React.Fragment>
            );
          })}
        </ul>
      </div>
    );
  }
}
export default Reg;
